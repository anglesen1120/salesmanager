create database MyPhamOnline
use MyPhamOnline
go

create table KhachHang(
user_KH nvarchar(50) not null primary key,
pass char(50) not null,
repass char(50) not null,
full_name nvarchar(100) not null,
phone char(11) not null,
mail varchar(50) not null,
address nvarchar(max) not null,
sex nvarchar(10) not null,
date_born char(11) not null,
status int not null
)

create table Quyen(
id int not null IDENTITY(1,1) primary key,
name nvarchar(100) not null,
detail nvarchar(max) not null
)

create table NhanVien(
user_NV nvarchar(50) not null primary key,
pass char(50) not null,
repass char(50) not null,
full_name nvarchar(100) not null,
phone char(11) not null,
mail varchar(50) not null,
address nvarchar(max) not null,
sex nvarchar(10) not null,
date_born char(11) not null,
permission_id int not null,
constraint fk_NhanVien foreign key (permission_id) references Quyen(id)
)

create table HoaDon(
bill_id int not null IDENTITY(1,1) primary key,
user_KH nvarchar(50) not null,
user_NV nvarchar(50) not null,
phone char(11) not null,
address nvarchar(max) not null,
date_receive char(11) not null,
date_order char(11) not null,
total int not null,
status int not null,
constraint fk_HoaDon foreign key (user_KH) references KhachHang(user_KH)
)

create table LoaiSanPham(
id int not null IDENTITY(1,1) primary key,
name nvarchar(max) not null,
description nvarchar(max) not null
)

create table ThuongHieu(
id int not null IDENTITY(1,1) primary key,
name nvarchar(max) not null
)

create table SanPham(
id int not null IDENTITY(1,1) primary key,
product_type_id int not null,
brand_id int not null,
constraint fk1_SanPham foreign key (product_type_id) references LoaiSanPham(id),
constraint fk2_SanPham foreign key (brand_id) references ThuongHieu(id),
name nvarchar(max) not null,
amount int not null,
price int not null,
description nvarchar(max) not null,
usage nvarchar(max) not null,
img nvarchar(max) not null
)

create table ChiTietHoaDon(
bill_id int not null,
product_id int not null,
constraint pk_ChiTietHoaDon primary key (bill_id, product_id),
constraint fk1_ChiTietHoaDon foreign key (bill_id) references HoaDon(bill_id),
constraint fk2_ChiTietHoaDon foreign key (product_id) references SanPham(id),
name nvarchar(max) not null,
amount int not null,
price int not null,
img nvarchar(200) not null
)
