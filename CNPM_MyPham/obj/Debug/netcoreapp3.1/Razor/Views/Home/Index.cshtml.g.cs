#pragma checksum "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "03aad100a0d55a237ef95c49364a2c3345bbb4c4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\_ViewImports.cshtml"
using CNPM_MyPham;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\_ViewImports.cshtml"
using CNPM_MyPham.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"03aad100a0d55a237ef95c49364a2c3345bbb4c4", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"405ffefc3cd786874472c586c5fa76147766a884", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IndexViewHomeModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            DefineSection("Stylesheet", async() => {
                WriteLiteral("\r\n");
            }
            );
            WriteLiteral("\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral(@"
    <script>
        function ShopThemSPAjax(idsp){
            $.ajax({
                url : ""/Shop/ShopThemSPAjax"",
                type : ""POST"",
                dataType: ""json"",
                data: {
                    idsp: idsp
                },
                async: false,
                success: function(data){
                    if(data != null){
                        Swal.fire({
                            type: ""success"",
                            title : data
                        }).then((result) => {
                            // layout.js
                            LoadCartForLayOut();
                        });
                    }
                },
                error: function(e){
                    Swal.fire({
                            type: ""error"",
                            title: ""Lỗi thêm sản phẩm => ShopThemSPAjax"",
                            html: e.responseText
                        });
                }
            });
   ");
                WriteLiteral("     }\r\n    </script>\r\n");
            }
            );
            WriteLiteral("\r\n<!-- Hero Section Begin -->\r\n");
            WriteLiteral("    <!-- Hero Section End -->\r\n\r\n    \r\n\r\n    <!-- Women Banner Section Begin -->\r\n    <section class=\"women-banner spad\">\r\n        <div class=\"container-fluid\">\r\n            <div class=\"row\">\r\n");
            WriteLiteral(@"                <div class=""col-lg-8 offset-lg-1"">
                    <div class=""filter-control"" style=""font-size: 130%; font-weight: bold;"">
                        Sản phẩm bán chạy
                    </div>
                    <div class=""product-slider owl-carousel"">
");
#nullable restore
#line 92 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                          
                            foreach(var sp in Model.ListSPChay){

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <div class=\"product-item\">\r\n                                    <div class=\"pi-pic\">\r\n                                        <img");
            BeginWriteAttribute("src", " src=\"", 3328, "\"", 3355, 1);
#nullable restore
#line 96 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
WriteAttributeValue("", 3334, Url.Content(@sp.img), 3334, 21, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("alt", " alt=\"", 3356, "\"", 3362, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n                                        <ul>\r\n                                            <li class=\"w-icon active\"><a");
            BeginWriteAttribute("onclick", " onclick=\"", 3484, "\"", 3524, 3);
            WriteAttributeValue("", 3494, "ShopThemSPAjax(", 3494, 15, true);
#nullable restore
#line 98 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
WriteAttributeValue("", 3509, sp.product_id, 3509, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3523, ")", 3523, 1, true);
            EndWriteAttribute();
            WriteLiteral("><i class=\"icon_bag_alt\"></i></a></li>\r\n                                            <li class=\"quick-view\"><a");
            BeginWriteAttribute("href", " href=\"", 3634, "\"", 3670, 2);
            WriteAttributeValue("", 3641, "/Product/Index/", 3641, 15, true);
#nullable restore
#line 99 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
WriteAttributeValue("", 3656, sp.product_id, 3656, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Xem chi tiết</a></li>\r\n                                        </ul>\r\n                                    </div>\r\n                                    <div class=\"pi-text\">\r\n");
#nullable restore
#line 103 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                                          
                                            foreach(var k in Model.ListTH){
                                                if(sp.brand_id == k.brand_id){

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                    <div class=\"catagory-name\">");
#nullable restore
#line 106 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                                                                          Write(k.name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n");
#nullable restore
#line 107 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                                                    break;
                                                }
                                            }
                                        

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        \r\n                                        <a");
            BeginWriteAttribute("href", " href=\"", 4425, "\"", 4461, 2);
            WriteAttributeValue("", 4432, "/Product/Index/", 4432, 15, true);
#nullable restore
#line 112 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
WriteAttributeValue("", 4447, sp.product_id, 4447, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                            <h5>");
#nullable restore
#line 113 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                                           Write(sp.name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h5>\r\n                                        </a>\r\n                                        <div class=\"product-price\">\r\n                                            ");
#nullable restore
#line 116 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                                        Write(String.Format("{0:n0}", sp.price));

#line default
#line hidden
#nullable disable
            WriteLiteral(" đ\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n");
#nullable restore
#line 120 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                            }
                        

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Women Banner Section End -->
    <!-- Man Banner Section Begin -->
    <section class=""man-banner spad"">
        <div class=""container-fluid"">
            <div class=""row"">
                <div class=""col-lg-8"">
                    <div class=""filter-control"" style=""font-size: 130%; font-weight: bold;"">
                        Sản phẩm hot
                    </div>
                    <div class=""product-slider owl-carousel"">
");
#nullable restore
#line 137 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                          
                            foreach(var sp in Model.ListSPNoi){

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <div class=\"product-item\">\r\n                                    <div class=\"pi-pic\">\r\n                                        <img");
            BeginWriteAttribute("src", " src=\"", 5726, "\"", 5753, 1);
#nullable restore
#line 141 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
WriteAttributeValue("", 5732, Url.Content(@sp.img), 5732, 21, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("alt", " alt=\"", 5754, "\"", 5760, 0);
            EndWriteAttribute();
            WriteLiteral(">\r\n                                        <ul>\r\n                                            <li class=\"w-icon active\"><a");
            BeginWriteAttribute("onclick", " onclick=\"", 5882, "\"", 5922, 3);
            WriteAttributeValue("", 5892, "ShopThemSPAjax(", 5892, 15, true);
#nullable restore
#line 143 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
WriteAttributeValue("", 5907, sp.product_id, 5907, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 5921, ")", 5921, 1, true);
            EndWriteAttribute();
            WriteLiteral("><i class=\"icon_bag_alt\"></i></a></li>\r\n                                            <li class=\"quick-view\"><a");
            BeginWriteAttribute("href", " href=\"", 6032, "\"", 6068, 2);
            WriteAttributeValue("", 6039, "/Product/Index/", 6039, 15, true);
#nullable restore
#line 144 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
WriteAttributeValue("", 6054, sp.product_id, 6054, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Xem chi tiết</a></li>\r\n                                        </ul>\r\n                                    </div>\r\n                                    <div class=\"pi-text\">\r\n");
#nullable restore
#line 148 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                                          
                                            foreach(var k in Model.ListTH){
                                                if(sp.brand_id == k.brand_id){

#line default
#line hidden
#nullable disable
            WriteLiteral("                                                    <div class=\"catagory-name\">");
#nullable restore
#line 151 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                                                                          Write(k.name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n");
#nullable restore
#line 152 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                                                    break;
                                                }
                                            }
                                        

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        \r\n                                        <a");
            BeginWriteAttribute("href", " href=\"", 6823, "\"", 6859, 2);
            WriteAttributeValue("", 6830, "/Product/Index/", 6830, 15, true);
#nullable restore
#line 157 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
WriteAttributeValue("", 6845, sp.product_id, 6845, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                            <h5>");
#nullable restore
#line 158 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                                           Write(sp.name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h5>\r\n                                        </a>\r\n                                        <div class=\"product-price\">\r\n                                            ");
#nullable restore
#line 161 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                                        Write(String.Format("{0:n0}", sp.price));

#line default
#line hidden
#nullable disable
            WriteLiteral(" đ\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n");
#nullable restore
#line 165 "E:\Project_FeeBlancer\salesmanager\CNPM_MyPham\Views\Home\Index.cshtml"
                            }
                        

#line default
#line hidden
#nullable disable
            WriteLiteral("                    </div>\r\n                </div>\r\n");
            WriteLiteral("            </div>\r\n        </div>\r\n    </section>\r\n    <!-- Man Banner Section End -->\r\n\r\n    ");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexViewHomeModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
